# gossiper

A **gossiper** is someone who talks eagerly and casually about other people. If you like to spread rumors and hear the latest news about your friends, you might be a **gossiper**.   
As it's name, **gossiper** will be act as a notification worker that will notify **sehat** users using email, sms, whatsapp, etc